/* global $ */

let AH = window.AH = window.AH || {}
// Quantity Selector | Turn into simple jquery plugin with some events
AH.initQuantitySelectors = function(){
  $(document).on('input', '.qty-selector .qty-select', function(){
    let value = parseInt(this.value),
        selectorEl = this.closest('.qty-selector'),
        inputEl = selectorEl.querySelector('.qty-input'),
        selectEl = selectorEl.querySelector('.qty-select'),
        max = selectorEl.dataset['max'] || 9;
   
    if ( selectorEl && inputEl ) {
      inputEl.value = value
      $(inputEl).trigger('change input')
      // $(inputEl).trigger('change')
      
      if ( value > max ) {
        inputEl.id = selectEl.id
        selectEl.id = ''
        selectorEl.classList.add('ten-plus')
        inputEl.select();
      } 
    }
  })
}

;(function(){

  const LOADER_CLASS = 'isLoading'
  const CRAWLER_LENGTH = 250

  function arrayFrom(items){
    if ( Array.isArray(items)) 
      return items
    if (typeof items === 'string') 
      return Array.from(document.querySelectorAll(items))
    else if ( items === undefined )
      return [document.body]
    else if (items.jquery) 
      return items.toArray()
    throw new Error('Loader parameter must be a selector or element(s)')
  }

  function createLoader(type = 'page') {
    var newLoader = document.createElement('div')
    
    if ( type === 'component' ) {
      newLoader.className = `ah-loader-scrim`
      newLoader.innerHTML = 
      `<svg class="ah-loader-outline-svg" width="100%" height="100%" viewBox="0 0 100 100" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg">
        <path class="ah-loader-path" stroke-width="5" d="M0,0 L 0,100 100,100 100,0 Z" vector-effect="non-scaling-stroke" fill-rule="evenodd"/>
      </svg>`
    } else if ( type === 'more' ) {
      newLoader.className = `ah-loader-more`
      newLoader.innerHTML = 
        `<span class="ah-loader-house"><svg class="icon"><use xlink:href="#house-icon"></use></svg></span>
        <span class="ah-loader-house"><svg class="icon"><use xlink:href="#house-icon"></use></svg></span>
        <span class="ah-loader-house"><svg class="icon"><use xlink:href="#house-icon"></use></svg></span>
        <span class="ah-loader-text">Loading More</span>`
    } else {
      newLoader.className = `ah-loader-scrim`
      newLoader.innerHTML = 
        `<div class="ah-loader" role="status" aria-live="polite">
          <span class="sr-only">Loading...</span>
          <div class="ah-loader-house"></div>
          <div class="ah-loader-shadow"></div>
        </div>`
    }
    return newLoader
  }

  function off(elements) {
    elements = arrayFrom(elements)
    elements.forEach( el => {
      var loader, loadingEl
      if ( el.classList.contains(LOADER_CLASS)) {
        loadingEl = el
        loader = el.querySelector('.ah-loader-scrim, .ah-loader-more')
      } else if ( el.classList.contains('ah-loader-scrim') || el.classList.contains('ah-loader-more') ) {
        loader = el 
        loadingEl = el.parent
      }
      if ( !loader ) return false
      loadingEl.classList.remove(LOADER_CLASS)
      loadingEl.removeChild(loader)
      return true
    })
  }
  
  function on(elements, type){
    elements = arrayFrom(elements)
    
    elements.forEach( el => {
      var loader = el.querySelector('.ah-loader-scrim, .ah-loader-more')
      if ( !loader ) {
        loader = createLoader(type)
        loader._loadingElement = el
        el.appendChild(loader)
        if ( type == 'component' ) {
          var length = el.clientWidth * 2 + el.clientHeight * 2;
          var path = loader.querySelector('.ah-loader-path')
          path.style.strokeDasharray = CRAWLER_LENGTH + ' ' + length;
        }
      }
      el.classList.add(LOADER_CLASS)
    })
    return { off: () => off(elements) }
  }

  function more(el){
    return on(el, 'more')
  }

  function component(el){
    return on(el, 'component')
  }
  function page(el){
    el = el || 'BODY'
    return on(el, 'page')
  }

  window.AH.loader = { 
    more: more,
    page: page, 
    component: component,
    on: on,
    off: off 
  }
})()

;(function(){
  function progress(button, state) {
    let clientRects
    if ( !button || !button.classList )
    if ( button.jquery ) button = button[0]
    clientRects = button.getBoundingClientRect()
    button.style.width = button.style.width || clientRects.width + "px"
    button.style.height = button.style.height || clientRects.height + "px"
    button.classList.remove('pop')
    switch (state) {
      case 'progress' || 'loading':
        button.disabled = true;
        button.classList.remove('isSuccessful')
        button.classList.remove('isErrored')
        button.classList.add('isProgressing')
        break;
      case 'success' || 'done':
        button.disabled = true;
        button.classList.remove('isProgressing')
        button.classList.remove('isErrored')
        button.classList.add('isSuccessful')
        break;
      case 'error':
        button.disabled = true;
        button.classList.remove('isProgressing')
        button.classList.remove('isSuccessful')
        button.classList.add('isErrored')
        break;
      case 'default':
      default:
        button.disabled = false;
        button.classList.remove('isProgressing')
        button.classList.remove('isSuccessful')
        button.classList.remove('isErrored')
    }
    
    if ( state && state !== 'default' ) {
      setTimeout(()=>{
        button.classList.add('pop')
      }, 30)
    }
  }
  window.AH.progress = progress
})()

;(function(){

  let AH_TRANSITION_PROPS = {
    'slide-in': ['transform','opacity'],
    'pop-in': ['transform','opacity'],
    'bounce-in': ['transform','opacity']
  } 

  function getTransitionProperties(transitionClass) { 
    let hidden = document.createElement('div'),
        testElement = document.createElement('div'),
        props;
    hidden.style.display = 'none'
    testElement = document.createElement('div')
    testElement.className = transitionClass
    hidden.appendChild(testElement)
    document.body.appendChild(hidden)
    props =  window.getComputedStyle(testElement).transitionProperty.split(/,\s?/g)
    document.body.removeChild(hidden)
    AH_TRANSITION_PROPS[transitionClass] = props
    return props
  }

  function ah_transition(elem){
    let $el = $(elem), el = $el[0], nt, count;
    
    if ( !el._transitionQueue || !el._transitionQueue.length ) {
      el._transitioning = false;
      $el.off('transitionend')
      return;
    }

    el._transitioning = true;
    nt = el._transitionQueue.shift()
    count = nt.props.length || 1

    if ( nt.direction === 'in' ) {
      // improve this.  display none?
      $el.removeClass('d-none d-lg-none d-sm-none d-xs-none d-md-none')
    }
    
    $el.on('transitionend', function(){
      if ( --count === 0 ) {
        $el.removeClass(nt.start)
        nt.callback && nt.callback()
        nt.deferred.done()
        ah_transition(el)
      }
    })

    $el.addClass( nt.start )
    setTimeout( ()=>{
      $el.addClass( nt.finish )
    }, 10) 
  }
  
  function transitionIn(elem, transition, callback){
    let $el = $(elem), el = $el[0];
    let transitionClass = transition.replace(/\s/g,'-')
    let deferred = $.Deferred()
    
    el._transitionQueue = el._transitionQueue || []
    
    el._transitionQueue.push({
      direction: 'in',
      props: AH_TRANSITION_PROPS[transitionClass] || getTransitionProperties(transitionClass),
      start: transitionClass,
      finish: transitionClass + '-fin',
      callback: callback,
      deferred: deferred
    })
    
    if ( !el._transitioning ) ah_transition(el)
  } 

  AH.transitionIn = transitionIn
  // AH.transitionOut = transitionOut
})()

// AH.phantom = function(el, newEl, callback){
  
// }

AH.initQuantitySelectors()




